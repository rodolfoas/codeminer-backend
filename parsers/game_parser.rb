require "./model/game"

class GameParser

  def self.parse(name,log)
    game = Game.new(name)
    log.each do |hash| 
        value = hash[:player_killed] == :world ? -1 : 1
        game.kills.has_key?(hash[:player]) ? game.kills[hash[:player]] += value : 
            game.kills[hash[:player]] = value
        game.kills_by_means.has_key?(hash[:kind_of_kill]) ? game.kills_by_means[hash[:kind_of_kill]] += 1 : 
            game.kills_by_means[hash[:kind_of_kill]] = 1
    end
    game
  end

end