require "./parsers/game_parser"

class QuakeLogParser
	
	attr_reader :path

	def initialize(path)
		@path = path
	end

	def parse
		return nil if @path.nil?
		games = []
		file.each_line do |line|
  			games << process_game("game-#{games.size+1}") if game_started?(line)
		end
		games
	end

	private 
	def file
	   @file ||= File.open(@path, "r")
	end 

	def process_game(name)
		game_log = []
  		while (line = file.gets)
  			if game_ended?(line)
  				return parse_game(name, game_log)
  			end
  			if has_killed?(line)
  				game_log << kill_parse(line)
  			end
  		end
	end

	def game_started?(line)
		/InitGame:/ =~ line
	end

	def game_ended?(line)
		/ShutdownGame:/ =~line
	end

	def has_killed?(line)
		/Kill:/ =~ line
	end

	def world_kills?(line)
		/: <world>/ =~ line
	end

	def kill_parse(line)
		world_kills?(line) ? parse_world_kills(line) : parse_user_kills(line)
	end

	def parse_user_kills(line)	
		{ :player => valid_index(/: ([A-Za-z ]+) killed/.match(line)),
		  :player_killed => valid_index(/killed ([A-Za-z ]+) by/.match(line)), 
		  :kind_of_kill => valid_index(/by ([A-Za-z_]+)/.match(line))
		}
	end

	def parse_world_kills(line)
		{ :player => valid_index(/<world> killed ([A-Za-z ]+) by/.match(line)),
		  :player_killed => :world,
		  :kind_of_kill => valid_index(/by ([A-Za-z_]+)/.match(line))
		}
	end

	def valid_index(result)
		result[1].strip
	end

	def parse_game(name, game_log)
		GameParser.parse(name, game_log)
	end


end