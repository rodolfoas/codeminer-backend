require "./parsers/quake_log_parser"

games = QuakeLogParser.new("games.log.txt")

kills_rank = {}

games.parse.each do |game|
	p game.to_json
	kills_rank = kills_rank.merge(game.rank){ |k,oVal,nVal| oVal+nVal } unless game.rank.empty?
end

p "Ranking: #{kills_rank}"