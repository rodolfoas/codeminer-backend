require "json"

class Game

	attr_accessor :name, :kills, :kills_by_means

	def initialize(name)
    	@name = name
		@kills = {}
		@players = {}
    	@kills_by_means = {}
    end

  	def to_json
     { @name.to_sym => { :total_kills => total_kills, :players => players, :kills => @killsm,
       :kills_by_means => @kills_by_means }}.to_json
  	end

 	def rank
      Hash[@kills.sort_by { |k,v| v }.reverse] unless @kills.nil?
  	end

    def total_kills
  	  @kills.inject(0) { |sum, tuple| sum += tuple[1] }
    end

    def players
     @kills.keys
    end

end